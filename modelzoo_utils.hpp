#pragma once
#include <runtime.h>
#include <icraft-dev/device.h>
#include <random>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <vector>
#include <fstream>
#include <random>

#ifdef __linux__
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/mman.h>
#include <unistd.h>
#endif

using namespace icraft::dev;
using namespace icraft::rt;
using namespace std::string_literals;
using namespace std::chrono;
using namespace std::chrono_literals;

//-------------------------------------//
//       AXI PLin通用
//-------------------------------------//
struct ConvertInfo {
	std::vector<int> dims;
	std::vector<std::pair<int, int>> scale;
	std::vector<float> norm_ratio;
	int bits;
};


/**
 * @description: 从names文件中读取每个index对应的标签名称
 * @param  name_path  names文件路径
 * @return 返回指向RuntineTensor的智能指针
 * @notes: 
 */
std::vector<std::string> getLabelName(const std::string& name_path) {

	std::ifstream names_stream(name_path);
	std::string line;
	std::vector<std::string> LABELS;

	while (std::getline(names_stream, line)) {
		LABELS.push_back(line);
	}

	return LABELS;
}


/**
 * @description: 从cv Mat构造RuntimeTensor
 * @param  mat   
 * @return 返回指向RuntineTensor的智能指针
 * @notes: 有一次数据拷贝的操作，可以考虑优化
 */
std::shared_ptr<icraft::rt::RuntimeTensor> fromCvMat(const cv::Mat& mat) {

	auto H = mat.rows;
	auto W = mat.cols;
	auto C = mat.channels();
	auto size = 1 * H * W * C;
	std::vector<int> dims{ 1, H, W, C };
	std::shared_ptr<uint8_t[]> data(new uint8_t[size]);
	std::copy_n((uint8_t*)mat.data, size, data.get());
	auto cv_tensor = std::make_shared<icraft::rt::RuntimeTensor>(data, dims);
	return cv_tensor;
}


/**
 * @description: 在不使用硬算子时，需要在runtimeapp中把硬件排布转换为软件排布
 * @param result_tensor  前向返回的RuntimeTensor
 * @param result_infos   从json中读取的维度信息
 * @param device         runtime device
 * @return {*}
 */
void dataFormatH2S(
    std::vector<std::shared_ptr<icraft::rt::RuntimeTensor>>& result_tensor, 
    const std::vector<ConvertInfo>& result_infos, 
    std::shared_ptr<icraft::dev::IcraftDevice> device) {
    int index = 0;
    for (auto info : result_infos) {
        icraft::rt::RuntimeTensor::ConverterInfo converter(info.dims, info.scale,info.norm_ratio,info.bits);
        result_tensor[index] = result_tensor[index]->toHost()->fmHwToSw(converter, device);
        index++;
    }
}


/**
 * @description: 从RuntimeNetwork中获取维度和量化参数信息
 * @param network_ptr   指向RuntimeNetwork的智能指针
 * @param infos         以引用的方式 返回存放信息的结构体
 * @return {*}
 */
void InfoFromOutput(const std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr, std::vector<ConvertInfo>& infos) {
    //TODO:判断output前是否有customop，如有则从customop中取值;
    auto ops = network_ptr->getOps();
    auto output = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[ops.size() - 1]);
    auto inputs_num = output->inputFtmp().size();
    auto inputs = output->inputFtmp();
    for (auto input : inputs) {
        ConvertInfo info;
        info.scale = input->scale();
        info.norm_ratio = input->normratio();
        info.bits = input->typeBits();
        auto dims = input->dim();
        auto c = input->channDistribution();
        info.dims = { dims[0],dims[2],dims[3],c[0].second}; //这里认为输出无效通道是在有效通道后的情况，没考虑间隔排布
        infos.push_back(info);
    }
}

 /**
 * @description: 从RuntimeNetwork中customop中获取量化的归一化参数信息
 * @param network_ptr   指向RuntimeNetwork的智能指针
 * @return 返回yolo多个head对应的不同的归一化参数
*/
std::vector<float> infoFromCustomop(const std::shared_ptr<icraft::rt::RuntimeNetwork>& network_ptr) {
    auto ops = network_ptr->getOps();
    auto output = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[ops.size() - 2]);
    auto inputs = output->inputFtmp();
    std::vector<float> normalratio;
    for (auto input : inputs) {
        auto norm_ratio = input->normratio()[0];
        normalratio.emplace_back(norm_ratio);    
    }
    return normalratio;
}



//-------------------------------------//
//       Yolo系列后处理相关
//-------------------------------------//
const int kColorMap[][3] = { {255, 128, 0}, {255, 153, 51}, {255, 178, 102}, {230, 230, 0}, {255, 153, 255}, 
                             {153, 204, 255}, {255, 102, 255}, {255, 51, 255}, {102, 178, 255},  {51, 153, 255}, 
                             {255, 153, 153}, {255, 102, 102},  {255, 51, 51}, {153, 255, 153}, {102, 255, 102},
                             {51, 255, 51}, {0, 255, 0}, {0, 0, 255}, {255, 0, 0},  {128, 255, 255} };


/**
 * @description: 简单版本的nms
 * @param box_list      推理出来的的bbox列表
 * @param socre_list    置信度列表
 * @param conf          筛选置信度阈值
 * @param iou           筛选的iou阈值
 * @return {*}
 * @notes: 该版本nms对于不同类的框也会做nms
 */
std::vector<int> simple_nms(std::vector<cv::Rect> box_list, std::vector<float> socre_list, float conf, float iou) {

	std::vector<int> nms_indices;
	std::vector<std::pair<float, int> > score_index_vec;


	for (size_t i = 0; i < socre_list.size(); ++i) {
		if (socre_list[i] > conf) {
			score_index_vec.emplace_back(std::make_pair(socre_list[i], i));
		}
	}

	   std::stable_sort(score_index_vec.begin(), score_index_vec.end(), 
        [](const std::pair<float, int>& pair1, const std::pair<float, int>& pair2){return pair1.first > pair2.first;});

	for (size_t i = 0; i < score_index_vec.size(); ++i) {
		const int idx = score_index_vec[i].second;
		bool keep = true;
		for (int k = 0; k < nms_indices.size() && keep; ++k) {
			if (1.f - jaccardDistance(box_list[idx], box_list[nms_indices[k]]) > iou) {
				keep = false;
			}
		}
		if (keep == true)
			nms_indices.emplace_back(idx);
	}

	return nms_indices;
}

/**
 * @description: 正确版本的nms
 * @param box_list      推理出来的的bbox列表
 * @param socre_list    置信度列表
 * @param conf          筛选置信度阈值
 * @param iou           筛选的iou阈值
 * @return {*}
 * @notes: 
 */
std::vector<int> nms(std::vector<cv::Rect>& box_list, std::vector<float>& socre_list, std::vector<int>& id_list, const float& conf, const float& iou, const int& NOC) {

	std::vector<int> nms_indices;
    for (int class_id = 0; class_id < NOC; class_id++) {
        std::vector<std::pair<float, int> > score_index_vec;

        for (size_t i = 0; i < socre_list.size(); ++i) {
		if (socre_list[i] > conf && id_list[i] == class_id) {
			score_index_vec.emplace_back(std::make_pair(socre_list[i], i));
		    }   
	    }
    
        std::stable_sort(score_index_vec.begin(), score_index_vec.end(), 
        [](const std::pair<float, int>& pair1, const std::pair<float, int>& pair2){return pair1.first > pair2.first;});

        for (size_t i = 0; i < score_index_vec.size(); ++i) {
            const int idx = score_index_vec[i].second;
            bool keep = true;
            for (int k = 0; k < nms_indices.size() && keep; ++k) {
                if (1.f - jaccardDistance(box_list[idx], box_list[nms_indices[k]]) > iou) {
                    keep = false;
                }
            }
            if (keep == true)
                nms_indices.emplace_back(idx);
        }
    }
	return nms_indices;
}


/**
 * @description:        在cvMat上绘制后处理过后的检测框
 * @param img
 * @param box_list      推理出来的的bbox列表
 * @param socre_list    置信度列表
 * @param id_list       id列表
 * @param nms_indices   最终筛选出来的结果的index
 * @return {*}
 * @notes: 
 */
void drawDetectBox(cv::Mat& img, std::vector<cv::Rect> box_list, std::vector<float> socre_list, std::vector<int> id_list, std::vector<int> nms_indices, std::vector<std::string>LABELS) {
	for (int i = 0; i < nms_indices.size(); ++i) {
		int index = nms_indices[i];
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
		float x1 = box_list[index].tl().x;
		float y1 = box_list[index].tl().y;
		float w = box_list[index].width;
		float h = box_list[index].height;
		cv::rectangle(img, cv::Rect(x1, y1, w, h), color_, 2);
		std::string s = LABELS[id_list[index]] + " : " + std::to_string(int(round(socre_list[index] * 100))) + "%";
		auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 1.0, 1, 0);
		cv::rectangle(img, cv::Point(x1, y1 - s_size.height - 5), cv::Point(x1 + s_size.width, y1), color_, -1);
		cv::putText(img, s, cv::Point(x1, y1 - 5), cv::FONT_HERSHEY_DUPLEX, 1.0, cv::Scalar(255, 255, 255), 1);
	}
}


/**
 * @description:        在cvMat上绘制信息
 * @param input_img     输入的cvMat
 * @param text          右上角信息，一般为fps数值
 * @param model_name    左上角信息，一般为模型名称
 * @param color         信息颜色
 * @return {*}
 * @notes: 
 */
void drawText(cv::Mat& input_img, const std::string& text, const std::string& model_name, cv::Scalar color){

	int font_face = cv::FONT_HERSHEY_COMPLEX;
	double font_scale = 2;
	int thickness = 2;
    int baseline;
    auto text_size = cv::getTextSize(text, font_face, font_scale, thickness, &baseline);
    cv::Point origin; 
    origin.x = 0;
    origin.y = 0;
	// origin.x = input_img.cols / 2 - text_size.width / 2;
	// origin.y = input_img.rows / 2 + text_size.height / 2;
    //cv::rectangle(input_img, origin, cv::Point(origin.x + text_size.width + 10, origin.y + text_size.height + 10), (0, 0, 0), -1);
	cv::putText(input_img, text, cv::Point(origin.x, origin.y + + text_size.height), font_face, font_scale, color, thickness);

    cv::Point model_name_pos;
    
    auto model_name_text_size = cv::getTextSize(model_name, font_face, font_scale, thickness, &baseline);
    model_name_pos.x = input_img.cols - model_name_text_size.width;
    model_name_pos.y = 0;
    //cv::rectangle(input_img, model_name_pos, cv::Point(model_name_pos.x + model_name_text_size.width, model_name_pos.y + model_name_text_size.height + 10), (0, 0, 0), -1);
	cv::putText(input_img, model_name, cv::Point(model_name_pos.x, model_name_pos.y + + model_name_text_size.height), font_face, font_scale, color, thickness);
}


/**
 * @description: 根据平台显示检测结果或者保存检测结果图
 * @param img
 * @param save_name
 * @return {*}
 * @notes: 在linux系统下没有gui界面，直接保存检测结果图
 */
void showImage(const cv::Mat& img, const char* save_name = " ") {

auto path = std::string(save_name);
std::string::size_type iPos = path.find_last_of('/') + 1;
std::string filename = path.substr(iPos, path.length() - iPos);
std::string name = filename.substr(0, filename.rfind("."));

#ifdef WIN32
	cv::imshow(name, img);
	cv::waitKey(0);
#else
    auto cv_saved_name = name + ".jpg";
	cv::imwrite(cv_saved_name, img);
    std::cout << "Saving result file: " << cv_saved_name << std::endl;
#endif
}

std::default_random_engine e;
std::uniform_int_distribution<unsigned> u(0, 255);
cv::Scalar randomColor() {
	return cv::Scalar(u(e), u(e), u(e));
}

cv::Scalar classColor(int id) {
    return cv::Scalar(kColorMap[id%20][0], kColorMap[id%20][1], kColorMap[id%20][2]);
}



//-------------------------------------//
//      其他
//-------------------------------------//
void FlipPer4(int16_t* tensor_data, int size) {
	for (int i = 0; i < size; i += 4) {
		int16_t a0 = tensor_data[i+0];
		int16_t a1 = tensor_data[i+1];
		int16_t a2 = tensor_data[i+2];
		int16_t a3 = tensor_data[i+3];

		tensor_data[i+3] = a0;
		tensor_data[i+2] = a1;
		tensor_data[i+1] = a2;
		tensor_data[i+0] = a3;
	}
}


inline float cal_distance(float x1, float y1, float x2, float y2)
{
    return abs(x2 - x1) + abs (y2 - y1);
};


template<typename T>
inline T abs_mean(T a1, T a2) {
    return abs((a1 + a2) / 2);
}


template<typename T>
inline float cal_dis(T x1, T y1, T x2, T y2) {
    return sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
}


template <typename T>
static inline float sigmoid(T const& x) {
    return (1. / (1. + exp(-x)));
}


template <typename T>
static inline std::vector<float> sigmoid(const std::vector<T>& x) {
    std::vector<float> res;
    for (auto i : x) {
        res.push_back(1. / (1. + exp(-i)));
    }
    return res;
}


template <typename T>
static inline std::vector<float> sigmoid(const T* x , int startptr,int calcnum) {
    std::vector<float> res;
    for (size_t i = startptr; i < (startptr + calcnum); i++)
    {
        res.push_back(1. / (1. + exp(-x[i])));
    }
    return res;
}


template <typename T, typename D>
static inline std::vector<float> sigmoid(const T* x, D alpha ,int startptr, int calcnum) {
    std::vector<float> res;
    for (size_t i = startptr; i < (startptr + calcnum); i++)
    {
        res.push_back(1. / (1. + exp(-x[i]* alpha)));
    }
    return res;
}



//-------------------------------------//
//       AXI 
//-------------------------------------//

/**
 * @description: axi模式中调用，从ps搬移数据到pl
 * @param device  runtime deivce的智能指针
 * @param dst_h   height
 * @param dst_w   width
 * @param dst_c   channel
 * @return {*}
 * @notes: 实际上是一个数据搬移的过程，并没有启动image make
 */
void forImkData(std::shared_ptr<icraft::dev::IcraftDevice> device, std::shared_ptr<icraft::rt::RuntimeTensor>& img_tensor, int dst_h, int dst_w, int dst_c = 3) {
	int demo_reg_base = 0x100C0000;
    // int demo_reg_base = 0x10000C00;

    uint64_t mapped_base;
    uint64_t zone_size;
    auto chunk_udma = device->getMM(icraft::dev::UDMABUF)->mallocMem(dst_h*dst_w*dst_c);

    auto ImageMakeRddrBase = chunk_udma->physAddr();

    auto ImageMakeChannel = dst_c;
    auto ImageMakeWidth = dst_w;
    auto ImageMakeHeight = dst_h;

    auto ImageMakeRlen = ((ImageMakeWidth * ImageMakeHeight - 1) / (24 / ImageMakeChannel) + 1) * 3;   //this 
    auto ImageMakeLastSft = ImageMakeWidth * ImageMakeHeight - (ImageMakeRlen - 3) / 3 * (24 / ImageMakeChannel);
    device->writeReg(demo_reg_base + 0x4, ImageMakeRddrBase, true);
    device->writeReg(demo_reg_base + 0x8, ImageMakeRlen, true);
    device->writeReg(demo_reg_base + 0xC, ImageMakeLastSft, true);
    device->writeReg(demo_reg_base + 0x10, ImageMakeChannel, true);
    device->writeReg(demo_reg_base + 0x1C, 1, true); // 1 -> data from hp
    device->writeReg(demo_reg_base + 0x20, 0, true);

    int img_size = ImageMakeWidth * ImageMakeHeight * ImageMakeChannel;
    auto tensor_data = img_tensor->data<uint8_t>(); //图片原始密排数据
    try {
        device->writeReservedMem((int8_t*)tensor_data.get(), 0, img_size);
    }
    catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }
    chunk_udma->free();
    //启动寄存器
    device->writeReg(demo_reg_base, 1, true);
}


//-------------------------------------//
//       PLin 
//-------------------------------------//
void hardResizePL(std::shared_ptr<icraft::dev::IcraftDevice> device, int CAMERA_W, int CAMERA_H, int NET_W, int NET_H)

{
    int RATIO_W = CAMERA_W / NET_W;
    int RATIO_H = CAMERA_H / NET_H;
    int IMG_W = RATIO_W * NET_W;
    int IMG_H = RATIO_H * NET_H;
    int BIAS_W = (CAMERA_W - IMG_W) / 2;
    int BIAS_H = (CAMERA_H - IMG_H) / 2;
       
    device->writeReg(0x40080018, 1);
    device->writeReg(0x40080020, RATIO_W);			// x方向行步长
    device->writeReg(0x40080024, RATIO_H);			// y方向列步长
	device->writeReg(0x40080028, BIAS_W);			// 起始x0 坐标位置 （0~FRAME_W）
	device->writeReg(0x4008002C, BIAS_H);			// 起始y0 坐标位置 （0~FRAME_H）
	device->writeReg(0x40080030, IMG_W + BIAS_W - 1);	    // 终止x1 坐标位置 （0~FRAME_W）
	device->writeReg(0x40080034, IMG_H + BIAS_H - 1);		// 终止y1 坐标位置 （0~FRAME_H）
	device->writeReg(0x40080038, CAMERA_W);	    // 图像X方向总长度 （FRAME_W）
	device->writeReg(0x4008003C, CAMERA_H);	    // 图像y方向总长度 （FRAME_H）
}


void hardResizePS(std::shared_ptr<icraft::dev::IcraftDevice> dev, const int CAMERA_WIDTH, const int CAMERA_HEIGHT, 
                       const int FRAME_WIDTH, const int FRAME_HEIGHT)
{

    int ws = CAMERA_WIDTH / FRAME_WIDTH;
    int hs = CAMERA_HEIGHT / FRAME_HEIGHT;
    int IMG_W = ws * FRAME_WIDTH;
    int IMG_H = hs * FRAME_HEIGHT;
    int x0 = (CAMERA_WIDTH - IMG_W) / 2;
    int y0 = (CAMERA_HEIGHT - IMG_H) / 2;

    auto x1 = CAMERA_WIDTH - x0 - 1;
    auto y1 = CAMERA_HEIGHT - y0 - 1;

    uint32_t PS_X0X1 = 0x4008005C;
    uint32_t PS_Y0Y1 = 0x40080060;
    uint32_t PS_XlYl = 0x40080064;
    uint32_t PS_WsHs = 0x40080068;
    uint32_t PS_SIZE = 0x4008006C;

    dev->writeReg(0x40080018, 1);
    dev->writeReg(PS_X0X1, x0 << 16 | x1);
    dev->writeReg(PS_Y0Y1, y0 << 16 | y1);
    dev->writeReg(PS_XlYl, CAMERA_WIDTH << 16 | CAMERA_HEIGHT);
    dev->writeReg(PS_WsHs, ws << 4 | hs);
    dev->writeReg(PS_SIZE, FRAME_WIDTH * FRAME_HEIGHT * 4 / 8);
}

template<typename predicate, typename Rep, typename Period>
bool WaitUntil(predicate check, std::chrono::duration<Rep, Period> timeout){
    auto start = std::chrono::steady_clock::now();
    while (!check()){
        if(timeout > 0ms && std::chrono::steady_clock::now() - start > timeout){return false;}
    }
    return true;
}


struct Buffer {
    uint64_t phys_addr;
    uint64_t offset;
    uint64_t size;
};


class DeviceMTSafe
{
public:
    DeviceMTSafe(std::shared_ptr<icraft::dev::IcraftDevice> device) 
        : device_(device) { 
    }

    std::pair<uint64_t, uint64_t> getReservedAddrSize() {
        auto reg_lock = std::unique_lock(get_mutex_);
        return device_->getReservedAddrSize();
    }

    void readReservedMem(int8_t *dest, uint64_t offset, uint64_t size) {
        auto reg_lock = std::unique_lock(dma_mutex_);
        device_->readReservedMem(dest, offset, size);
    }

    void writeReg(uint64_t addr, uint32_t data, bool relative = false) {
        auto reg_lock = std::unique_lock(reg_mutex_);
        device_->writeReg(addr, data);
    }

    uint32_t readReg(uint64_t addr, bool relative = false) {
        auto reg_lock = std::unique_lock(reg_mutex_);
        return device_->readReg(addr);
    }
    
private:
    std::shared_ptr<icraft::dev::IcraftDevice> device_;
    inline static std::mutex reg_mutex_;
    inline static std::mutex dma_mutex_;
    inline static std::mutex get_mutex_;
};


class Camera {
public:
    Camera() = default;

    Camera(std::shared_ptr<icraft::dev::IcraftDevice> device, uint64_t buffer_size, uint64_t mem_offset = 0) 
       : buffer_size_(buffer_size), device_(device)
    {

        // device_ = std::make_shared<DeviceMTSafe>(device);
        auto [raddr, rsize] = device_->getReservedAddrSize();
        buffer_ = Buffer{(raddr + mem_offset) >> 3, mem_offset, buffer_size_};
    }

    void get(int8_t* frame) const{
        device_->readReservedMem(frame, buffer_.offset, buffer_size_);
    }

    void take() const {
        device_->writeReg(CAMERA_WRITE_ADDR, buffer_.phys_addr);
        device_->writeReg(CAMERA_TAKE, 1);
    }

    void take(int write_ddr, int take_ddr) const {
        // auto camera_done = device_->readReg(CAMERA_DONE);  
        // std::cout << "Before take, Camera_done=0x" << std::hex << camera_done  << std::dec <<  '\n';
        device_->writeReg(write_ddr, buffer_.phys_addr);
        device_->writeReg(take_ddr, 1);
        // camera_done = device_->readReg(CAMERA_DONE);  
        // std::cout << "After take, Camera_done=0x" << std::hex << camera_done  << std::dec <<  '\n';
    }

    void readWriteTest() {
        device_->writeReg(0x40080000, 0xff);
        auto read = device_->readReg(0x40080000);
        std::cout << "read result=" << std::hex << read << '\n';
    }

    bool wait() const {
        auto error = false;
        WaitUntil(
            [&]() -> bool{
                auto camera_done = device_->readReg(CAMERA_DONE);
                error = camera_done & 0x4;
                return (camera_done & 0x1) == 1;
            },
            20ms
        );
        return error == 0;
    }

    bool wait(int index) const {
        auto error = false;
        WaitUntil(
            [&]() -> bool{
                auto camera_done = device_->readReg(CAMERA_DONE);
                error = camera_done & (index << 8);
                return (camera_done & index) == index;
            },
            20ms
        );
        return error == 0;
    }
    

    const Buffer& buffer() const {
        return buffer_;
    }

private:
    // std::shared_ptr<DeviceMTSafe> device_;
    std::shared_ptr<icraft::dev::IcraftDevice> device_;
    Buffer buffer_;
    uint64_t buffer_size_ = 0;

    const static auto CAMERA_TAKE = 0x40080004;
    const static auto CAMERA_WRITE_ADDR = 0x40080050;
    const static auto CAMERA_DONE = 0x40080058;
};


class Display_old {
public:
    Display_old() = default;

    Display_old(std::shared_ptr<icraft::dev::IcraftDevice> device, uint64_t buffer_size, uint64_t mem_offset) 
        : device_(device), buffer_size_(buffer_size) {
        auto [raddr, rsize] = device_->getReservedAddrSize();
        buffer_ = Buffer{(raddr + mem_offset) >> 3, mem_offset, buffer_size_};
    }

    void show(int8_t* frame) const {
        device_->writeReservedMem(frame, buffer_.offset, buffer_size_);
        device_->writeReg(DISPLAY_READ_ADDR, buffer_.phys_addr);
    }

private:
    std::shared_ptr<icraft::dev::IcraftDevice> device_;
    Buffer buffer_;
    uint64_t buffer_size_ = 0;

    const static auto DISPLAY_READ_ADDR = 0x40080054;


};

#ifdef __linux__
/**
 *  Hdmi显示抽象类
 */
class Display {
public:
    Display() = default;

    Display(const char* dev) 
    {
        int ret = 0;
        fd_ = open(dev, O_RDWR);
        if (fd_ < 0) {
            printf("open device [%s] failed:%s\n", dev, strerror(errno));
	    }

        ret = ioctl(fd_, FBIOGET_FSCREENINFO, &fix);    
        if (ret < 0) {
            printf("read fb device fscreeninfo failed:%s\n", strerror(errno));
            close(fd_);
        }

        ret = ioctl(fd_, FBIOGET_VSCREENINFO, &var);
        if (ret < 0) {
            printf("read fb device vscreeninfo failed:%s\n", strerror(errno));
            close(fd_);
        }

        mem_size_ = var.xres * var.yres * var.bits_per_pixel / 8;	/* 计算内存 */
	    ptr_buf = (uint8_t *)mmap(NULL, mem_size_, PROT_READ|PROT_WRITE, MAP_SHARED, fd_, 0);
        if (ptr_buf == NULL) {
            printf("fb device mmap failed:%s\n", strerror(errno));
            close(fd_);
        }
        
        memset(ptr_buf, 0, mem_size_);  // 清除屏幕
    }

    ~Display() {
        
        munmap(ptr_buf, mem_size_);
	    close(fd_);
    }

    void show(int8_t* frame) const {
        memcpy(ptr_buf, frame, mem_size_);
    }


    void draw_top_left(int8_t* frame) {
        uint8_t *poffset_buf = ptr_buf;
        for (int col = 0; col < var.yres; ++col) {
             memcpy(poffset_buf, frame, var.xres / 2);
             poffset_buf += var.xres;
             frame += var.xres / 2;
        }
    }

    void draw_top_right(int8_t* frame) {
        uint8_t *poffset_buf = ptr_buf + var.xres / 2;
        for (int col = 0; col < var.yres; ++col) {
             memcpy(poffset_buf, frame, var.xres / 2);
             poffset_buf += var.xres;
             frame += var.xres / 2;
        }
    }

    void draw_bottom_left(int8_t* frame) {
        uint8_t *poffset_buf = ptr_buf + var.yres * var.xres / 2;
        for (int col = 0; col < var.yres; ++col) {
             memcpy(poffset_buf, frame, var.xres / 2);
             poffset_buf += var.xres;
             frame += var.xres / 2;
        }
    }

    void draw_bottom_right(int8_t* frame) {
        uint8_t *poffset_buf = ptr_buf + var.yres * var.xres / 2 + var.xres / 2;
        for (int col = 0; col < var.yres; ++col) {
             memcpy(poffset_buf, frame, var.xres / 2);
             poffset_buf += var.xres;
             frame += var.xres / 2;
        }
    }

    void draw_pixel(int x, int y, uint32_t color)
    {
        uint8_t *poffset_buf = NULL;

        poffset_buf = ptr_buf + (x * var.bits_per_pixel/8) 
                    + (y * var.xres * var.bits_per_pixel/8);	/* 计算内存偏移地址 */
        *(uint32_t*)poffset_buf = color;	/* ARGB32格式 */

    }

    void fill_pixel(uint32_t color)
    {
        int i, j;

        for (i=0; i < var.xres; i ++) 
        {
            for (j=0; j < var.yres; j ++) 
            {
                draw_pixel(i, j, color);
            }
        }
    }

    uint8_t* getPtr() const { return ptr_buf; }

private:
    uint8_t *ptr_buf;
    int fd_;
    int mem_size_;
    struct fb_fix_screeninfo fix;
	struct fb_var_screeninfo var;/* framebuffer设备信息*/

};

class DisplayRange{
public:
    DisplayRange(int startrow, int endrow, int startcol, int endcol, const cv::Mat& mat)
        :startrow_(startrow), endrow_(endrow), startcol_(startcol), endcol_(endcol) {
        mat_ = mat.rowRange(startrow, endrow).colRange(startcol, endcol);
    }

    const cv::Mat& mat() const { return mat_; }

    const int startrow() const {return startrow_; }
    const int endrow() const {return endrow_; }
    const int startcol() const {return startcol_; }
    const int endcol() const {return endcol_; }

private:
    int startrow_;
    int endrow_;
    int startcol_;
    int endcol_;
    cv::Mat mat_;
};

std::pair<std::shared_ptr<Camera>, std::shared_ptr<Display>> setUpCameraAndSceen(std::shared_ptr<icraft::dev::IcraftDevice> dev, const uint64_t BUFFER_SIZE)
{
    auto [raddr, rsize] = dev->getReservedAddrSize();
    auto mem_offset = rsize;
    mem_offset -= BUFFER_SIZE;
    auto camera = std::make_shared<Camera>(dev, BUFFER_SIZE, mem_offset);
    mem_offset -= BUFFER_SIZE;
    auto display = std::make_shared<Display>("/dev/fb0");

    return std::make_pair(camera, display);
}

#endif